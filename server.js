//the server.js class
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
//used to help implement css
app.use(express.static(__dirname + '/public'));
//this function uses a get method to retrieve the html file that will show up when webpage loads.
app.get('/', function (req, res) {
        res.sendFile(__dirname + '/public/chatPage.html')
    })
    //this function runs when a new client has connected to the webpage, as well as disconnect
io.on('connection', function (client) {
    console.log('A Client Has Connected');
    client.on('disconnect', function () {
        console.log('A Client Has Disconnected');
    });
    client.on('createMessage', function (msg, user) {
        io.emit('receiveMessage', msg);
        console.log('user: ' + msg);
    });
    client.on('createUser', function (name) {
        users.push(name);
    });
});
//this function starts the web server as well as socket.io server listening on 3000
server.listen(3000, function () {
    console.log('listening on *:3000');
});